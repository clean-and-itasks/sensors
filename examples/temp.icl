module temp

import StdEnv

import Sensors.iTasks.DHT

import iTasks

Start w = doTasks viewTemp w

viewTemp = withDHT 0 \dht->
	temperature dht -&&- humidity dht
