implementation module Sensors.I2C.BH1750

import StdEnv

import Data.Error
import Data.Tuple
import Sensors.I2C

//instance toInt BH1750Mode
//where
//	toInt ContinuousHighRes = 0x10
//	toInt ContinuousHighRes2 = 0x11
//	toInt ContinuousLowRes = 0x13
//	toInt OneTimeHighRes = 0x20
//	toInt OneTimeHighRes2 = 0x21
//	toInt OneTimeLowRes = 0x23

BH1750_CONV_FACTOR :== 1.2

//bh1750open :: !I2CDevice !BH1750Mode !*World -> *(!MaybeError String (), !*World)
bh1750open :: !I2CDevice !*World -> *(!MaybeError String (), !*World)
bh1750open i2c w
	//# (merr, w) = bh1750configure i2c mode w
	//| isError merr = (merr, w)
	//# (merr, w) = bh1750setMReg i2c 69 w
	//= bh1750configure i2c mode w
	// set mode
	# (merr, w) = i2cwrite i2c
		{#toChar ((8<<3) bitor (69>>5))
		, toChar ((3<<5) bitor (69 bitand 31))} w
	| isError merr = (Error ("bh1750open: " +++ toString (fromError merr)), w)
	# (_, w) = sleep 1 w
	# (merr, w) = i2cwrite i2c {#toChar 0x20} w
	| isError merr = (Error ("bh1750open: " +++ toString (fromError merr)), w)
	= (Ok (), snd (sleep 1 w))
where
	sleep :: !Int !*World -> *(!Int, !*World)
	sleep _ _ = code {
		ccall sleep "I:I:A"
	}


//bh1750configure :: !I2CDevice !BH1750Mode !*World -> *(!MaybeError String (), !*World)
//bh1750configure i2c mode w
//	# (merr, w) = i2cwrite i2c {#toChar (toInt mode)} w
//	| isError merr = (Error (toString (fromError merr)), w)
//	= (Ok (), w)

//bh1750setMReg :: !I2CDevice !Int !*World -> *(!MaybeError String (), !*World)
//bh1750setMReg i2c reg w
//	# (merr, w) = i2cwrite i2c {#toChar (toInt mode)} w
//	| isError merr = (Error (toString (fromError merr)), w)
//	= (Ok (), w)

bh1750value :: !I2CDevice !*World -> *(!MaybeError String Real, !*World)
bh1750value i2c w
	# (merr, w) = i2cread i2c 2 w
	| isError merr = (Error (toString (fromError merr)), w)
	# (Ok buf) = merr
	# level = toReal ((toInt buf.[0]<<8)+toInt buf.[1]) / 2.0 / BH1750_CONV_FACTOR
	= (Ok level, w)

bh1750close :: !I2CDevice !*World -> *(!MaybeOSError (), !*World)
bh1750close i2c w = i2cclose i2c w
