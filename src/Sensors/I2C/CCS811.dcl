definition module Sensors.I2C.CCS811

from Sensors.I2C import :: I2CDevice
from Data.Error import :: MaybeError
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorMessage, :: OSErrorCode

CCS811DEFAULTADDR :== 0x5B

:: CCS811

/**
 * Open a ccs811 device
 * @param device
 * @param world
 * @result Maybe an error
 */
ccs811open :: !*I2CDevice -> *MaybeError *(String, !*I2CDevice) *CCS811

/**
 * Read the value of a ccs811 device
 * Note that this errors quite often because reading i2c devices on non-rt
 * linux is tricky but errors can mostly be ignored and retried
 *
 * @param device
 * @param world
 * @result Maybe an error or a Co2 and TVOC measure
 */
ccs811values :: !*CCS811 -> *(!MaybeError String (Int, Int), !*CCS811)

/**
 * Calibrate the ccs811 sensor with environmental data
 *
 * @param temperature
 * @param humidity
 * @param world
 * @result Maybe an error or a Co2 and TVOC measure
 */
ccs811calibrate :: !Real !Real !*CCS811 -> *(!MaybeError String (), !*CCS811)

/**
 * Close a ccs811 device
 * @param device
 * @param world
 * @result Maybe an error
 */
ccs811close :: !*CCS811 -> *(!MaybeOSError (), !*I2CDevice)
