implementation module Sensors.I2C.CCS811

import StdEnv

import Data.Error
import Data.Tuple
import Sensors.I2C

:: CCS811 :== I2CDevice

ccs811open :: !*I2CDevice -> *MaybeError *(String, !*I2CDevice) *CCS811
ccs811open i2c
	// ask hw id
	# (merr, i2c) = i2cwrite {#toChar 0x20} i2c
	| isError merr = Error (toString (fromError merr), i2c)
	# (merr, i2c) = i2cread 1 i2c
	| isError merr = Error (toString (fromError merr), i2c)
	| (fromOk merr).[0] <> toChar 0x81
		= Error ("ID doesn't match, wrong device?", i2c)
	// app start
	# (merr, i2c) = i2cwrite {#toChar 0xf4} i2c
	| isError merr = Error (toString (fromError merr), i2c)
	// meas_mode constant power mode, no interrputs
	# (merr, i2c) = i2cwrite {#toChar 0x01,toChar 0x10} i2c
	| isError merr = Error (toString (fromError merr), i2c)
	= Ok i2c

ccs811values :: !*CCS811 -> *(!MaybeError String (Int, Int), !*CCS811)
ccs811values i2c
	# (merr, i2c) = i2cwrite {#toChar 0x02} i2c
	| isError merr = (Error (toString (fromError merr)), i2c)
	# (merr, i2c) = i2cread 5 i2c
	| isError merr = (Error (toString (fromError merr)), i2c)
	# buf = {#toInt c\\c<-:fromOk merr}
	// error
	| buf.[4] bitand 0x01 > 0
		# (merr, i2c) = i2cwrite {#toChar 0xe0} i2c
		| isError merr = (Error (toString (fromError merr)), i2c)
		# (merr, i2c) = i2cread 1 i2c
		| isError merr = (Error (toString (fromError merr)), i2c)
		= (Error ("Device error: " +++ toString (toInt (fromOk merr).[0])), i2c)
	// ok
	| buf.[4] bitand 0x99 == 0x98
		# (co2, tvoc) = ((buf.[0] << 8) + buf.[1], (buf.[2] << 8) + buf.[3])
		| co2 > 2000 || tvoc > 2000
			//(Error "Values out of range", w)
			= ccs811values i2c
		= (Ok (co2, tvoc), i2c)
	// unknown
	| otherwise
		= (Error "Invalid response", i2c)

ccs811calibrate :: !Real !Real !*CCS811 -> *(!MaybeError String (), !*CCS811)
ccs811calibrate temp humid i2c
	# temp = toInt ((temp - 25.0)*512.0)
	# humid = toInt (humid * 512.0)
	# (merr, i2c) = i2cwrite
		{#toChar 0x05
		, toChar ((humid >> 8) bitand 0xff)
		, toChar (humid bitand 0xff)
		, toChar ((temp >> 8) bitand 0xff)
		, toChar (temp bitand 0xff)
		} i2c
	| isError merr = (Error (toString (fromError merr)), i2c)
	= (Ok (), i2c)

ccs811close :: !*CCS811 -> *(!MaybeOSError (), !*I2CDevice)
ccs811close i2c
	// meas_mode idle
	= i2cwrite {#toChar 0x01,toChar 0x00} i2c
