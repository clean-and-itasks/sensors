definition module Sensors.I2C.BH1750

from Sensors.I2C import :: I2CDevice
from Data.Error import :: MaybeError
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorMessage, :: OSErrorCode

BH1750DEFAULTADDR :== 0x23
//:: BH1750Mode = ContinuousHighRes | ContinuousHighRes2 | ContinuousLowRes | OneTimeHighRes | OneTimeHighRes2 | OneTimeLowRes2

/**
 * Open a BH1750 device
 * @param device
 * @param mode
 * @param world
 * @result Maybe an error
 */
bh1750open :: !I2CDevice !*World -> *(!MaybeError String (), !*World)

/**
 * Read the value of a bh1750 device
 * Note that this errors quite often because reading i2c devices on non-rt
 * linux is tricky but errors can mostly be ignored and retried
 *
 * @param device
 * @param world
 * @result Maybe an error or a Co2 and TVOC measure
 */
bh1750value :: !I2CDevice !*World -> *(!MaybeError String Real, !*World)

/**
 * Close a bh1750 device
 * @param device
 * @param world
 * @result Maybe an error
 */
bh1750close :: !I2CDevice !*World -> *(!MaybeOSError (), !*World)
