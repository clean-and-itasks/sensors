definition module Sensors.I2C.CCS811.Task

from iTasks.SDS.Definition import :: SDSSource
from iTasks.WF.Definition import :: Task, class iTask
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
from iTasks.SDS.Definition import class RWShared, class Readable, class Writeable, class Modifiable, class Registrable, class Identifiable

from System.Time import :: Timespec

from Sensors.I2C import :: I2CDevice

CCS811Task :: !Int !Int !Timespec (sds () a (Int, Int)) -> Task () | RWShared sds & TC a
