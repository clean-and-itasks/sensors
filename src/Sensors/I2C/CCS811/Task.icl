implementation module Sensors.I2C.CCS811.Task

import StdEnv
import iTasks
import Data.Either
import Data.Tuple
import StdDebug

import iTasks.Internal.Util
import iTasks.Internal.TaskEval
import iTasks.Internal.IWorld

import Sensors.I2C
import Sensors.I2C.CCS811

:: *Resource | CCS811Resource Int Int *CCS811

CCS811Task :: !Int !Int !Timespec (sds () a (Int, Int)) -> Task () | RWShared sds & TC a
CCS811Task channel addr poll sds = Task evalinit
where
	evalinit :: !Event !TaskEvalOpts !*IWorld -> *(TaskResult (), !*IWorld)
	evalinit event evalOpts iworld
		| not (trace_tn "evalinit") = undef
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
		= case liftIWorld (i2copen channel addr) iworld of
			(Error e, iworld) = (ExceptionResult (exception e), iworld)
			(Ok i2c, iworld) = case ccs811open i2c of
				Error (err, i2c) = case liftIWorld (i2cclose i2c) iworld of
					(Error e, iworld) = (ExceptionResult (exception e), iworld)
					(Ok (), iworld) = (ExceptionResult (exception err), iworld)
				Ok i2c = eval event evalOpts {iworld & resources=[CCS811Resource channel addr i2c:iworld.resources]}

	withI2C f iworld
		# (mi2c, iworld) = iworldResource (\t=:(CCS811Resource c a _)->(c == channel && a == addr, t)) iworld
		= case mi2c of
			[] = (ExceptionResult (exception "This ccs811 was already closed"), iworld)
			[_,_:_]  = (ExceptionResult (exception "Multiple matching resources"), iworld)
			[i2c] = f i2c iworld

	eval :: !Event !TaskEvalOpts -> (*IWorld -> *(TaskResult (), *IWorld))
	eval event evalOpts=:{TaskEvalOpts|taskId,lastEval}
		| not (trace_tn "eval") = undef
		| isDestroyOrInterrupt event
			= withI2C \(CCS811Resource _ _ i2c) iworld
				= (DestroyedResult, closeI2C i2c iworld)
		| not (isRefreshForTask event taskId)
			= \iworld->(ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task eval), iworld)
		= withI2C \(CCS811Resource _ _ i2c) iworld
			| not (trace_tn "in withi2c") = undef
			# (merr, iworld) = readRegister taskId clock iworld
			| isError merr = (ExceptionResult (fromError merr), closeI2C i2c iworld)
			| not (trace_tn "readregister") = undef
			# (merr, i2c) = ccs811values i2c
			// Ignore errors
			| isError merr = (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task eval), {iworld & resources=[CCS811Resource channel addr i2c:iworld.resources]})
			| not (trace_tn "read values") = undef
			# (merr, iworld) = write (fromOk merr) sds EmptyContext iworld
			| isError merr = (ExceptionResult (fromError merr), closeI2C i2c iworld)
			| not (trace_tn "done") = undef
			= (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task eval), {iworld & resources=[CCS811Resource channel addr i2c:iworld.resources]})

	closeI2C :: !*CCS811 !*IWorld -> *IWorld
	closeI2C i2c iworld
		| not (trace_tn "closeI2C") = undef
		# (_, i2c) = ccs811close i2c
		# (_, iworld) = liftIWorld (i2cclose i2c) iworld
		= iworld

	rep = stringDisplay ("CCS811Sensor: " +++ toString addr)
	clock = sdsFocus {start=zero,interval=poll} iworldTimespec
