implementation module Sensors.IIO

import StdEnv
import Data.Error
import Data.GenHash
import System.OSError
import System.FilePath

derive gHash IIODev

instance toString IIODev
where
	toString (IIODev n) = "/sys/bus/iio/devices/iio:device" +++ toString n

IIORead :: !IIODev !String !(*File -> *(Bool, a, *File)) !*env -> *(!MaybeOSError a, !*env) | FileSystem env
IIORead dev path fun world
	# (ok, f, world) = fopen (toString dev </> path) FReadText world
	| not ok = getLastOSError world
	# (ok, i, f) = fun f
	| not ok
		# (merr, world) = getLastOSError world
		# (_, world) = fclose f world
		= (merr, world)
	# (ok, world) = fclose f world
	| not ok = getLastOSError world
	= (Ok i, world)

readCCS811eCO2 :: !IIODev !*env -> *(!MaybeOSError Int, !*env) | FileSystem env
readCCS811eCO2 dev w = IIORead dev "in_concentration_co2_raw" freadi w

readCCS811VOC :: !IIODev !*env -> *(!MaybeOSError Int, !*env) | FileSystem env
readCCS811VOC dev w = IIORead dev "in_concentration_voc_raw" freadi w

readDHT11temp :: !IIODev !*env -> *(!MaybeOSError Real, !*env) | FileSystem env
readDHT11temp dev w = IIORead dev "in_temp_input" readDHTReal w

readDHT11relhumid :: !IIODev !*env -> *(!MaybeOSError Real, !*env) | FileSystem env
readDHT11relhumid dev w = IIORead dev "in_humidityrelative_input" readDHTReal w

readDHTReal f :== let (ok, r, f`) = freadr f in (ok, r / 1000.0, f`)

readBH1750illuminance:: !IIODev !*env -> *(!MaybeOSError Int, !*env) | FileSystem env
readBH1750illuminance dev w = IIORead dev "in_illuminance_raw" freadi w

readMCP320xVoltage :: !IIODev !*env -> *(!MaybeOSError Int, !*env) | FileSystem env
readMCP320xVoltage dev w = IIORead dev "in_voltage0_raw" freadi w
