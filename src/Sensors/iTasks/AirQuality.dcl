definition module Sensors.iTasks.AirQuality

from iTasks.WF.Definition import :: Task

import Sensors.iTasks.AirQuality.CCS811IIO
import Sensors.iTasks.AirQuality.CCS811I2C

class AirQuality conf ~dev where
	withAirQuality :: conf ((dev conf) -> Task a) -> Task a
	eCO2 :: (dev conf) -> Task Int
	TVOC :: (dev conf) -> Task Int
