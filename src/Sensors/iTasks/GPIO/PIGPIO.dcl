definition module Sensors.iTasks.GPIO.PIGPIO

from Sensors.GPIO.PIGPIO import :: PiGPIO
from Sensors.iTasks.GPIO import class GPIO

:: PIGPIORef a (=: PIGPIORef PiGPIO)
instance GPIO () PiGPIO
