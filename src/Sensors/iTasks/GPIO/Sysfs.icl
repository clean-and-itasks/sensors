implementation module Sensors.iTasks.GPIO.Sysfs

import StdEnv
import iTasks
import Data.Tuple
import Sensors.GPIO
import Sensors.iTasks.GPIO

import Sensors.GPIO.Sysfs

:: GPIOSysfsHandle a = GPIOHandle

instance GPIO [Int] GPIOSysfsHandle
where
	withGPIO i f = withCleanupHook cleanup (setup >-| f GPIOHandle)
		where
			cleanup = sequence [accWorldError (GPIOUnexport i) toString\\i<-i]
			setup = sequence [accWorldError (GPIOExport i) toString\\i<-i]
	readD _ p = accWorldError (GPIOGet p) toString @ fromInt
	writeD _ p l = accWorldError (GPIOSet p (toInt l)) toString @ fromInt

instance GPIOPinMode [Int] GPIOSysfsHandle
where
	pinMode _ p m = accWorldError (GPIOMode p m) toString @! m
