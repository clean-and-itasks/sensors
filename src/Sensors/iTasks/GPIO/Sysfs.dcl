definition module Sensors.iTasks.GPIO.Sysfs

from Sensors.iTasks.GPIO import class GPIO, class GPIOPinMode
from Sensors.GPIO import :: PinMode

:: GPIOSysfsHandle a
instance GPIO [Int] GPIOSysfsHandle
instance GPIOPinMode [Int] GPIOSysfsHandle
