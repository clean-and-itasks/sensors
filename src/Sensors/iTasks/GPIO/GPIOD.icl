implementation module Sensors.iTasks.GPIO.GPIOD

import StdEnv
import iTasks
import Data.Tuple
import Sensors.GPIO
import Sensors.iTasks.GPIO

import Sensors.GPIO.GPIOD

:: GPIODHandle a = GPIOHandle !String

instance GPIO String GPIODHandle
where
	withGPIO i f = f (GPIOHandle i)
	readD (GPIOHandle dev) p = accWorldError (GPIOGet dev p) toString @ fromInt
	writeD (GPIOHandle dev) p l = accWorldError (GPIOSet dev p (toInt l)) toString @ fromInt
