definition module Sensors.iTasks.AirQuality.CCS811I2C

from Sensors.I2C import :: I2CDeviceOpts
from Sensors.iTasks.AirQuality import class AirQuality

:: CCS811Ref a (=: CCS811Ref I2CDeviceOpts)
instance AirQuality I2CDeviceOpts CCS811Ref
