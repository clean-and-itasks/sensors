implementation module Sensors.iTasks.AirQuality.CCS811IIO

import StdEnv
import Data.Func
import Data.Tuple
import iTasks

import Sensors.IIO
import Sensors.iTasks.AirQuality

instance AirQuality Int IIOHandle
where
	withAirQuality i f = f (IIOHandle (IIODev i))
	eCO2 (IIOHandle d) = viewSharedInformation [ViewAs \s->toString s +++ "ppm"]
		$ sdsFocus d $ sdsEverySecond co2
	TVOC (IIOHandle d) = viewSharedInformation [ViewAs \s->toString s +++ "ppb"]
		$ sdsFocus d $ sdsEverySecond voc

co2 :: SDSSource IIODev Int ()
co2 = worldShare (errToString o readCCS811eCO2) (\_ () w->(Ok (), w)) (\_ _ _->False)

voc :: SDSSource IIODev Int ()
voc = worldShare (errToString o readCCS811VOC) (\_ () w->(Ok (), w)) (\_ _ _->False)

sdsEverySecond :: !(sds p r w) -> SDSLens p r w | gHash{|*|} p & TC p & TC r & TC w & RWShared sds
sdsEverySecond sds = mapRead fst
	$ sds >*| sdsFocus () currentTimestamp

errToString fun w = case fun w of
	(Error e, w) = (Error (toString e), w)
	(Ok a, w) = (Ok a, w)
