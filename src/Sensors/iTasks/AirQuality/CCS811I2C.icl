implementation module Sensors.iTasks.AirQuality.CCS811I2C

import StdEnv
import Data.Func
import Data.Tuple
import iTasks
import iTasks.Internal.Util
import iTasks.Internal.IWorld
import iTasks.Internal.Task
import iTasks.Internal.TaskEval

import Sensors.I2C
import Sensors.I2C.CCS811
import Sensors.iTasks.AirQuality

:: CCS811Ref a =: CCS811Ref I2CDeviceOpts
:: *Resource | CCS811Resource I2CDeviceOpts *CCS811
instance AirQuality I2CDeviceOpts CCS811Ref
where
	withAirQuality i f = withCCS811 i f
	eCO2 (CCS811Ref d) = (Task (values d) @ fst) -|| watch currentTimestamp
	TVOC (CCS811Ref d) = (Task (values d) @ snd) -|| watch currentTimestamp

values :: I2CDeviceOpts Event TaskEvalOpts *IWorld -> *(TaskResult (Int, Int), *IWorld)
values d event opts=:{lastEval} iworld
	| isDestroyOrInterrupt event = (DestroyedResult, iworld)
	= case iworldResource (cmp d) iworld of
		([CCS811Resource opts ccs811], iworld)
			# (merr, ccs811) = ccs811values ccs811
			# iworld & resources = [CCS811Resource opts ccs811:iworld.resources]
			| isError merr = (ExceptionResult (exception (fromError merr)), iworld)
			= (ValueResult (Value (fromOk merr) False)
				(mkTaskEvalInfo lastEval)
				(mkUIIfReset event ui)
				(Task (values d))
			, iworld)
		([], iworld) = (ExceptionResult (exception "CCS811 resource gone"), iworld)
		(_, iworld) = (ExceptionResult (exception "Multiple CCS811 resources"), iworld)
where
	ui = stringDisplay $ "CCS811 on channel " +++ toString d.channel +++ " and address " +++ toString d.address
				
withCCS811 :: I2CDeviceOpts ((CCS811Ref I2CDeviceOpts) -> Task a) -> Task a
withCCS811 i f = Task evalinit
where
//	evalinit :: Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)
	evalinit event opts iworld
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
		= case liftIWorld (i2copen i) iworld of
			(Error e, iworld) = (ExceptionResult (exception e), iworld)
			(Ok i2c, iworld) = case ccs811open i2c of
				Error (e, i2c) = case liftIWorld (i2cclose i2c) iworld of
					(Error _, iworld) = (ExceptionResult (exception e), iworld)
					(Ok _, iworld) = (ExceptionResult (exception e), iworld)
				Ok ccs811
					# iworld & resources = [CCS811Resource i ccs811:iworld.resources]
					# (val, iworld) = apTask (f (CCS811Ref i)) event opts iworld
					= (wrapTaskContinuation eval val, iworld)

	eval :: !(Task a) Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)
	eval task event opts iworld = case apTask task event opts iworld of
		(ValueResult tv ei ui t, iworld)
			= (ValueResult tv ei ui (Task (eval t)), iworld)
		(ExceptionResult e, iworld) = case iworldResource (cmp i) iworld of
			([CCS811Resource opts ccs811], iworld)
				# (merr, i2c) = ccs811close ccs811
				| isError merr = (ExceptionResult e, snd $ liftIWorld (i2cclose i2c) iworld)
				# (merr, iworld) = liftIWorld (i2cclose i2c) iworld
				| isError merr = (ExceptionResult e, iworld)
				= (ExceptionResult e, iworld)
			([], iworld) = (ExceptionResult (exception "CCS811 resource gone"), iworld)
			(_, iworld) = (ExceptionResult (exception "Multiple CCS811 resources"), iworld)
		(DestroyedResult, iworld) = case iworldResource (cmp i) iworld of
			([CCS811Resource opts ccs811], iworld)
				# (merr, i2c) = ccs811close ccs811
				| isError merr = (ExceptionResult (exception (fromError merr)), snd $ liftIWorld (i2cclose i2c) iworld)
				# (merr, iworld) = liftIWorld (i2cclose i2c) iworld
				| isError merr = (ExceptionResult (exception (fromError merr)), iworld)
				= (DestroyedResult, iworld)
			([], iworld) = (ExceptionResult (exception "CCS811 resource gone"), iworld)
			(_, iworld) = (ExceptionResult (exception "Multiple CCS811 resources"), iworld)

cmp :: !I2CDeviceOpts !*Resource -> (!Bool, !*Resource)
cmp i (CCS811Resource opts dev)
	= (opts.channel == i.channel && opts.address == i.address, CCS811Resource opts dev)
cmp i r = (False, r)
