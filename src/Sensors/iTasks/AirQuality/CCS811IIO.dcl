definition module Sensors.iTasks.AirQuality.CCS811IIO

from Sensors.IIO import :: IIOHandle
from Sensors.iTasks.AirQuality import class AirQuality

instance AirQuality Int IIOHandle
