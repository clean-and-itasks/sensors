implementation module Sensors.iTasks.LightIntensity.BH1750IIO

import StdEnv
import Data.Func
import Data.Tuple
import iTasks

import Sensors.IIO
import Sensors.iTasks.LightIntensity

instance LightIntensity Int IIOHandle
where
	withLightIntensity i f = f (IIOHandle (IIODev i))
	illuminance (IIOHandle d) = (viewSharedInformation [ViewAs \s->toString s +++ "lx"]
		$ sdsFocus d $ sdsEverySecond illum) @ toReal

illum :: SDSSource IIODev Int ()
illum = worldShare (errToString o readBH1750illuminance) (\_ () w->(Ok (), w)) (\_ _ _->False)

sdsEverySecond :: !(sds p r w) -> SDSLens p r w | gHash{|*|} p & TC p & TC r & TC w & RWShared sds
sdsEverySecond sds = mapRead fst
	$ sds >*| sdsFocus () currentTimestamp

errToString fun w = case fun w of
	(Error e, w) = (Error (toString e), w)
	(Ok a, w) = (Ok a, w)
