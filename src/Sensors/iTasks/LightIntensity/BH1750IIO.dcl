definition module Sensors.iTasks.LightIntensity.BH1750IIO

from Sensors.IIO import :: IIOHandle
from Sensors.iTasks.LightIntensity import class LightIntensity

instance LightIntensity Int IIOHandle
