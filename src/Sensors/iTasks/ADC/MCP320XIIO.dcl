definition module Sensors.iTasks.ADC.MCP320XIIO

from Sensors.IIO import :: IIOHandle
from Sensors.iTasks.ADC import class ADC

instance ADC Int IIOHandle
