implementation module Sensors.iTasks.ADC.MCP320XIIO

import StdEnv
import Data.Func
import Data.Tuple
import iTasks

import Sensors.IIO
import Sensors.iTasks.ADC

instance ADC Int IIOHandle
where
	withADC i f = f (IIOHandle (IIODev i))
	voltage (IIOHandle d) = viewSharedInformation [ViewAs toString]
		$ sdsFocus d $ sdsEverySecond voltage

voltage :: SDSSource IIODev Int ()
voltage = worldShare (errToString o readMCP320xVoltage) (\_ () w->(Ok (), w)) (\_ _ _->False)

sdsEverySecond :: !(sds p r w) -> SDSLens p r w | gHash{|*|} p & TC p & TC r & TC w & RWShared sds
sdsEverySecond sds = mapRead fst
	$ sds >*| sdsFocus () currentTimestamp

errToString fun w = case fun w of
	(Error e, w) = (Error (toString e), w)
	(Ok a, w) = (Ok a, w)
