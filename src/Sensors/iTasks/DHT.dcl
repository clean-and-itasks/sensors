definition module Sensors.iTasks.DHT

from iTasks.WF.Definition import :: Task

import Sensors.iTasks.DHT.DHT11IIO

class DHT conf ~dev where
	withDHT :: conf ((dev conf) -> Task a) -> Task a
	temperature :: (dev conf) -> Task Real
	humidity :: (dev conf) -> Task Real
