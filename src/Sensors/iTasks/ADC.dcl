definition module Sensors.iTasks.ADC

from iTasks.WF.Definition import :: Task

import Sensors.iTasks.ADC.MCP320XIIO

class ADC conf ~dev where
	withADC :: conf ((dev conf) -> Task a) -> Task a
	voltage :: (dev conf) -> Task Int
