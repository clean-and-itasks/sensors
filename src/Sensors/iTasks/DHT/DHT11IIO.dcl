definition module Sensors.iTasks.DHT.DHT11IIO

from Sensors.IIO import :: IIOHandle
from Sensors.iTasks.DHT import class DHT

instance DHT Int IIOHandle
