implementation module Sensors.iTasks.DHT.DHT11IIO

import StdEnv
import Data.Tuple
import Data.Func
import iTasks

import Sensors.IIO
import Sensors.iTasks.DHT

instance DHT Int IIOHandle
where
	withDHT i f = f (IIOHandle (IIODev i))
	temperature (IIOHandle d)
		= viewSharedInformation [ViewAs \s->toString s +++ "°C"]
		$ mapRead fst $ sdsFocus d IIODHTShare
	humidity (IIOHandle d)
		= viewSharedInformation [ViewAs \s->toString s +++ "%"]
		$ mapRead snd $ sdsFocus d IIODHTShare

IIODHTShare :: SDSLens IIODev (Real, Real) ()
IIODHTShare = sdsEverySecond $ temp |*| humid
where
	temp = worldShare (dhtErr 3 o readDHT11temp) (\_ () w->(Ok (), w)) \_ _ _->False
	humid = worldShare (dhtErr 3 o readDHT11relhumid) (\_ () w->(Ok (), w)) \_ _ _->False

	dhtErr 0 fun w = case fun w of
		(Ok a, w) = (Ok a, w)
		(Error e, w) = (Error $ toString e, w)
	dhtErr try fun w = case fun w of
		(Ok a, w) = (Ok a, w)
		(Error (5, _), w) = dhtErr try fun w
		//We only try a connection timed out a limited number of times
		(Error (110, _), w) = dhtErr (dec try) fun w
		(Error e, w) = (Error $ toString e, w)

sdsEverySecond :: !(sds p r w) -> SDSLens p r w | gHash{|*|} p & TC p & TC r & TC w & RWShared sds
sdsEverySecond sds = mapRead fst $ sds >*| sdsFocus () currentTimestamp

errToString fun w = case fun w of
	(Error e, w) = (Error $ toString e, w)
	(Ok a, w) = (Ok a, w)
