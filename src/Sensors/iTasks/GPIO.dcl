definition module Sensors.iTasks.GPIO

from iTasks.WF.Definition import :: Task, class iTask
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONDecode, generic JSONEncode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: EditorPurpose, :: Editor
from Sensors.GPIO import :: Level(..), :: Pin(..), :: Pulldown(..)

import Sensors.iTasks.GPIO.Sysfs
import Sensors.iTasks.GPIO.GPIOD

derive class iTask Level, PinMode, Pulldown

class GPIO conf ~dev where
	withGPIO :: conf ((dev conf) -> Task a) -> Task a | iTask a
	readD :: (dev conf) Pin -> Task Level
	writeD :: (dev conf) Pin Level -> Task Level

class GPIOPinMode conf ~dev | GPIO conf dev where
	pinMode :: (dev conf) Pin PinMode -> Task PinMode

class GPIOPulldown conf ~dev | GPIO conf dev where
	pullDown :: (dev conf) Pulldown -> Task Pulldown
