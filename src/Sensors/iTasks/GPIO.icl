implementation module Sensors.iTasks.GPIO

import iTasks

import Sensors.GPIO

derive class iTask Level, PinMode, Pulldown
