definition module Sensors.iTasks.LightIntensity

from iTasks.WF.Definition import :: Task

import Sensors.iTasks.LightIntensity.BH1750IIO

class LightIntensity conf ~dev where
	withLightIntensity :: conf ((dev conf) -> Task a) -> Task a
	illuminance :: (dev conf) -> Task Real
