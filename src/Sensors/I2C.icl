implementation module Sensors.I2C

import StdEnv
import Data.Error
import System.OSError
import System._Posix
import System._Pointer

:: I2CDevice = I2CDevice Int

getFd :: *I2CDevice -> *(Int, *I2CDevice)
getFd (I2CDevice i2c) = (i2c, I2CDevice i2c)

I2C_SLAVE :== 0x703

i2copen :: !I2CDeviceOpts !*World -> *(!MaybeOSError *I2CDevice, !*World)
i2copen {channel,address} w
	# (fd, w) = opens ("/dev/i2c-" +++ toString channel) O_RDWR w
	| fd == -1 = getLastOSError w
	# (r, w) = ioctl fd I2C_SLAVE address w
	| r == -1 = getLastOSError w
	= (Ok (I2CDevice fd), w)

i2cwrite :: !{#Char} !*I2CDevice -> *(!MaybeOSError (), !*I2CDevice)
i2cwrite buf i2c
	# (fd, i2c) = getFd i2c
	# (r, i2c) = write fd buf (size buf) i2c
	| r == -1 = getLastOSError i2c
	= (Ok (), i2c)

i2cread :: !Int !*I2CDevice -> *(!MaybeOSError {#Char}, !*I2CDevice)
i2cread num i2c
	# (fd, i2c) = getFd i2c
	# (p, i2c) = mallocSt num i2c
	| p == 0 = getLastOSError i2c
	# (r, i2c) = read fd p num i2c
	| r == -1 = getLastOSError (freeSt p i2c)
	| r <> num = getLastOSError (freeSt p i2c)
	# (d, p) = readP (\p->derefCharArray p num) p
	= (Ok d, freeSt p i2c)

i2cclose :: !*I2CDevice !*World -> *(!MaybeOSError (), !*World)
i2cclose (I2CDevice fd) w
	# (r, w) = close fd w
	| r == -1 = getLastOSError w
	= (Ok (), w)
