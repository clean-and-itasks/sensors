implementation module Sensors.GPIO.Sysfs

import StdEnv
import System.OSError
import Data.Functor
import Data.Tuple
import Text
import Sensors.GPIO

root :== "/sys/class/gpio"
pin i :== root +++ "/gpio" +++ toString i
value i :== pin i +++ "/value"
mode i :== pin i +++ "/direction"
export :== root +++ "/export"
unexport :== root +++ "/unexport"

readFile :: !String !*World -> (!MaybeOSError String, !*World)
readFile fp w
	# (ok, f, w) = fopen fp FReadData w
	| not ok = getLastOSError w
	# (cs, (merr, f)) = readCompletely f
	| isError merr
		# (_, w) = fclose f w
		= (liftError merr, w)
	# (ok, w) = fclose f w
	| not ok = getLastOSError w
	= (Ok {c\\c<-cs}, w)
where
	readCompletely :: !*File -> (![Char], (MaybeOSError (), !*File))
	readCompletely f
		# (ok, c, f) = freadc f
		| not ok = ([], getLastOSError f)
		# (cs, (merr, f)) = readCompletely f
		= ([c:cs], (merr, f))

writeFile :: !String !String !*World -> (!MaybeOSError (), !*World)
writeFile fp data w
	# (ok, f, w) = fopen fp FWriteData w
	| not ok = getLastOSError w
	# f = fwrites data f
	//Check error manually because clean doesn't check this
	= case getLastOSError w of
		(Ok (), w)
			# (ok, w) = fclose f w
			| not ok = getLastOSError w
			= (Ok (), w)
		(Error (0, _), w)
			# (ok, w) = fclose f w
			| not ok = getLastOSError w
			= (Ok (), w)
		(Error e, w) = (Error e, w)

GPIOExport :: !Pin !*World -> (!MaybeOSError (), !*World)
GPIOExport p w = writeFile export (toString p) w

GPIOUnexport :: !Pin !*World -> (!MaybeOSError (), !*World)
GPIOUnexport p w = writeFile unexport (toString p) w

GPIOSet :: !Pin !Int !*World -> (!MaybeOSError Int, !*World)
GPIOSet p v w = appFst (fmap (const v)) (writeFile (value p) (toString v) w)

GPIOGet :: !Pin !*World -> (!MaybeOSError Int, !*World)
GPIOGet p w = appFst (fmap toInt) (readFile (value p) w)

GPIOMode :: !Pin !PinMode !*World -> (!MaybeOSError PinMode, !*World)
GPIOMode p v w
	# m = case v of
		PMInput = Ok "out"
		PMOutput = Ok "in"
		_ = Error (-1, "Unsupported pin mode")
	| isError m = (liftError m, w)
	= appFst (fmap (const v)) (writeFile (mode p) (fromOk m) w)
