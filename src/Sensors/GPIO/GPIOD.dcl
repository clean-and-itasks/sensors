definition module Sensors.GPIO.GPIOD

from Data.Error import :: MaybeError
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorCode, :: OSErrorMessage

from Sensors.GPIO import :: Pin, :: PinMode

GPIOSet :: !String !Pin !Int !*World -> (!MaybeOSError Int, !*World)
GPIOGet :: !String !Pin !*World -> (!MaybeOSError Int, !*World)
