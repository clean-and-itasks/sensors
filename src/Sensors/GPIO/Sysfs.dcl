definition module Sensors.GPIO.Sysfs

from Data.Error import :: MaybeError
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorCode, :: OSErrorMessage

from Sensors.GPIO import :: Pin, :: PinMode

GPIOExport :: !Pin !*World -> (!MaybeOSError (), !*World)
GPIOUnexport :: !Pin !*World -> (!MaybeOSError (), !*World)

GPIOSet :: !Pin !Int !*World -> (!MaybeOSError Int, !*World)
GPIOGet :: !Pin !*World -> (!MaybeOSError Int, !*World)
GPIOMode :: !Pin !PinMode !*World -> (!MaybeOSError PinMode, !*World)
