implementation module Sensors.GPIO.GPIOD

import StdEnv
import System.OSError
import Data.Functor
import Data.Tuple
import System._Pointer
import Text
import Sensors.GPIO

import code from library "-lgpiod"

consumer :: String
consumer =: packString "Sensors.GPIO.GPIODUsr"

GPIOSet :: !String !Pin !Int !*World -> (!MaybeOSError Int, !*World)
GPIOSet dev pin val w
	# (r, w) = gpiod_ctxless_set_value (packString dev) pin val True consumer 0 0 w
	| r == -1 = getLastOSError w
	= (Ok r, w)
where
	gpiod_ctxless_set_value :: !String !Int !Int !Bool !String !Pointer !Pointer !*World -> (!Int, !*World)
	gpiod_ctxless_set_value _ _ _ _ _ _ _ _ = code {
			ccall gpiod_ctxless_get_value "sIIIspp:I:A"
		}

GPIOGet :: !String !Pin !*World -> (!MaybeOSError Int, !*World)
GPIOGet dev pin w
	# (r, w) = gpiod_ctxless_get_value (packString dev) pin True consumer w
	| r == -1 = getLastOSError w
	= (Ok r, w)
where
	gpiod_ctxless_get_value :: !String !Int !Bool !String !*World -> (!Int, !*World)
	gpiod_ctxless_get_value _ _ _ _ _ = code {
			ccall gpiod_ctxless_get_value "sIIs:I:A"
		}
