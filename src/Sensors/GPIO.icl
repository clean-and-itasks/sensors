implementation module Sensors.GPIO

import StdEnv
import Text.GenPrint

derive gPrint Level, PinMode, Pulldown

instance toString Level where toString a = printToString a
instance toString PinMode where toString a = printToString a
instance toString Pulldown where toString a = printToString a
instance toInt PinMode
where
	toInt PMInput = 0
	toInt PMOutput = 1
	toInt PMAlt0 = 4
	toInt PMAlt1 = 5
	toInt PMAlt2 = 6
	toInt PMAlt3 = 7
	toInt PMAlt4 = 3
	toInt PMAlt5 = 2
instance fromInt PinMode
where
	fromInt 0 = PMInput
	fromInt 1 = PMOutput
	fromInt 4 = PMAlt0
	fromInt 5 = PMAlt1
	fromInt 6 = PMAlt2
	fromInt 7 = PMAlt3
	fromInt 3 = PMAlt4
	fromInt 2 = PMAlt5
instance toInt Pulldown
where
	toInt PDOff = 0
	toInt PDDown = 1
	toInt PDUp = 2
instance fromInt Pulldown
where
	fromInt 0 = PDOff
	fromInt 1 = PDDown
	fromInt 2 = PDUp
instance toInt Level
where
	toInt Low = 0
	toInt High = 1
instance fromInt Level
where
	fromInt 0 = Low
	fromInt 1 = High
