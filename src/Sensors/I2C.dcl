definition module Sensors.I2C

from Data.Error import :: MaybeError
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorMessage, :: OSErrorCode

:: I2CDeviceOpts = { channel :: !Int, address :: !Int }
:: I2CDevice = I2CDevice Int

i2copen :: !I2CDeviceOpts !*World -> *(!MaybeOSError *I2CDevice, !*World)

i2cwrite :: !{#Char} !*I2CDevice -> *(!MaybeOSError (), !*I2CDevice)
i2cread :: !Int !*I2CDevice -> *(!MaybeOSError {#Char}, !*I2CDevice)

i2cclose :: !*I2CDevice !*World -> *(!MaybeOSError (), !*World)
