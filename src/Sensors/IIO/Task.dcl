definition module Sensors.IIO.Task

//from iTasks.SDS.Definition import :: SDSSource
//from iTasks.WF.Definition import :: Task, class iTask
//from Data.GenEq import generic gEq
//from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
//from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
//from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose
//from iTasks.SDS.Definition import class RWShared, class Readable, class Writeable, class Modifiable, class Registrable, class Identifiable, :: SDSLens
from iTasks.SDS.Definition import :: SDSLens

from Sensors.IIO import :: IIODev
from Data.GenHash import generic gHash

derive gHash IIODev
IIODHTShare :: SDSLens IIODev (Real, Real) ()
