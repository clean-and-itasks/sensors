implementation module Sensors.IIO.Task

import StdEnv
import Data.Tuple
import Data.Func
import Data.GenHash
import Sensors.IIO
import iTasks

derive gHash IIODev

IIODHTShare :: SDSLens IIODev (Real, Real) ()
IIODHTShare = sdsEverySecond (temp |*| humid)
where
	temp = worldShare (dhtErr o readDHT11temp) (\_ () w->(Ok (), w)) (\_ _ _->False)
	humid = worldShare (dhtErr o readDHT11relhumid) (\_ () w->(Ok (), w)) (\_ _ _->False)

	dhtErr fun w = case fun w of
		(Ok a, w) = (Ok a, w)
		(Error (5, _), w) = dhtErr fun w
		(Error (110, _), w) = dhtErr fun w
		(Error e, w) = (Error (toString e), w)

sdsEverySecond :: !(sds p r w) -> SDSLens p r w | gHash{|*|} p & TC p & TC r & TC w & RWShared sds
sdsEverySecond sds = mapRead fst
	$ sds >*| sdsFocus () currentTimestamp

errToString fun w = case fun w of
	(Error e, w) = (Error (toString e), w)
	(Ok a, w) = (Ok a, w)
