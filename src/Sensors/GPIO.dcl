definition module Sensors.GPIO

from StdOverloaded import class toString, class toInt, class fromInt
from Data.Error import :: MaybeError

:: PinMode = PMInput | PMOutput | PMAlt0 | PMAlt1 | PMAlt2 | PMAlt3 | PMAlt4 | PMAlt5
:: Pulldown = PDOff | PDDown | PDUp
:: Level = High | Low
:: Pin :== Int

instance toString Level, PinMode, Pulldown
instance toInt PinMode, Pulldown, Level
instance fromInt PinMode, Pulldown, Level
