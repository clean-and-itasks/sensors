definition module Sensors.IIO

from StdOverloaded import class toString
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorCode, :: OSErrorMessage
from Data.Error import :: MaybeError
from StdFile import class FileSystem
from Data.GenHash import generic gHash

:: IIODev =: IIODev Int
:: IIOHandle a =: IIOHandle IIODev
instance toString IIODev

derive gHash IIODev

IIORead :: !IIODev !String !(*File -> *(Bool, a, *File)) !*env -> *(!MaybeOSError a, !*env) | FileSystem env

readCCS811eCO2 :: !IIODev !*env -> *(!MaybeOSError Int, !*env) | FileSystem env
readCCS811VOC :: !IIODev !*env -> *(!MaybeOSError Int, !*env) | FileSystem env

readDHT11temp :: !IIODev !*env -> *(!MaybeOSError Real, !*env) | FileSystem env
readDHT11relhumid :: !IIODev !*env -> *(!MaybeOSError Real, !*env) | FileSystem env

readBH1750illuminance:: !IIODev !*env -> *(!MaybeOSError Int, !*env) | FileSystem env

readMCP320xVoltage :: !IIODev !*env -> *(!MaybeOSError Int, !*env) | FileSystem env
