## Setup the DHT and mpc3002

Overlays are included by default in raspbian. Add to /boot/config.txt

```
dtoverlay=dht11,gpiopin=4
dtoverlay=mcp3008,spi0-0-present
```

## To setup the IIO drivers for not included sensors (ccs811, bh1750)

Compile the kernel:

```
git clone https://github.com/raspberrypi/linux
cd linux
KERNEL=kernel8
make bcm2711_defconfig
make menuconfig
# enable for sensors:
# Device Drivers->Industrial I/O support->Chemical Sensors->AMS CCS811 VOC sensor
# Device Drivers->Industrial I/O support->Light sensors->ROHM BH1750 ambient light sensors
# Device Drivers->Industrial I/O support->Analog to digital converters->Microchip Technology MCP3x01/02/04/08 and MCP3550/1/3
make -j4 modules dtbs
make -j4
```

Install the new kernel

```
# install the new kernel
sudo make modules_install
sudo make install
sudo cp arch/arm64/boot/Image /boot/kernel8_custom.img
sudo cp arch/arm64/boot/dts/broadcom/*.dtb /boot
sudo cp arch/arm64/boot/dts/overlays/*.dtb* /boot/overlays
sudo cp arch/arm64/boot/dts/overlays/READBE /boot/overlays

# Use the new kernel
echo "kernel=kernel8_custom.img" >> /boot/config.txt
```

### Enable them manually

Enable the modules

```
echo "ccs811" | sudo tee -o /etc/modules
echo "bh1750" | sudo tee -o /etc/modules
```

At runtime add the devices:

```
echo "ccs811 0x5b" | sudo tee /sys/bus/i2c/devices/i2c-1/new_device
echo "bh1750 0x23" | sudo tee /sys/bus/i2c/devices/i2c-1/new_device
```

This will add the devices as:

```
/sys/bus/iio/devices/iio:deviceN
```

### Using overlays

Checkout the patched `arch/arm/boot/dts/overlays/i2c-sensor-overlay.dts` from here:
https://github.com/dopefishh/linux/blob/add-bh1750-ccs811/arch/arm/boot/dts/overlays/i2c-sensor-overlay.dts

And compile it and copy it to /boot/overlays

```
cd arch/arm/boot/dts/overlays
dtc -@ -I dts -O dtb -o i2c-sensor.dtbo i2c-sensor-overlay.dts
cp i2c-sensor-mod.dtbo /boot/overlays/
```

Enable it in the `/boot/config.txt` by adding the following line:

```
dtoverlay=i2c-sensor,ccs811,bh1750
```

## To setup hwmon drivers (sht3x)

hwmon drivers come installed in the kernel already and can be enabled by
enabling the modules:

```
echo "sht3x" | sudo tee -o /etc/modules
```

At runtime add the devices:

```
echo "sht3x 0x45" | sudo tee /sys/bus/i2c/devices/i2c-1/new_device
```

This will add the device as:

```
/sys/class/hwmon/hwmonN
```
