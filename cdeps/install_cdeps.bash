#!/bin/bash
mkdir -p ../src/Sensors/Clean\ System\ Files

# pigpio
make -C pigpio
if [ $(id -u) -eq 0 ]
then
	make -C pigpio install
else
	sudo make -C pigpio install
fi
